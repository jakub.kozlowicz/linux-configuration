# JK Linux configuration

This is repository containing all my configuration files for most used
programs, which I install on every Linux I am currently using.

List of programs in this repo:

- Bash
- Doom Emacs
- Git
- Kitty
- Neovim
- Vim
- Zsh

## Usage

To use this configuration files you have to use `stow`. This is program to
manage farms of symbolic links.

```bash
https://gitlab.com/jakub.kozlowicz/Dotfiles.git
cd Dotfiles
stow */
```

**The slash after wildcard is important because it will only take folders from
this repo without README or .gitignore file.**

There is also possibility to use only specific configuration file. For that
use:

```bash
stow [name of folder]
```
