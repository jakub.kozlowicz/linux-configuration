#!/bin/bash
# vim:fileencoding=utf-8:ft=bash:foldmethod=marker

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# EXPORTS ----------------------------------------------------------------- {{{

export TERM="xterm-256color"                                            # Getting proper terminal behaviour
export HISTORY_IGNORE="(ls|cd|pwd|exit|sudo reboot|history|cd -|cd ..)" # Ignore some commands in history
export EDITOR="nvim"                                                    # $EDITOR use vim in terminal
export VISUAL="nvim"                                                    # $VISUAL use emacs in GUI mode
export NPM_CONFIG_PREFIX="$HOME/.npm_global"

# }}}

# PATH -------------------------------------------------------------------- {{{

if [ -d "$HOME/.bin" ]; then
	PATH="$HOME/.bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ]; then
	PATH="$HOME/.local/bin:$PATH"
fi

if [ -d "$HOME/.npm_global/bin" ]; then
	PATH="$HOME/.npm_global/bin:$PATH"
fi

# }}}

# BOOKMARKS --------------------------------------------------------------- {{{

if [ -d "$HOME/.bookmarks" ]; then
	export CDPATH=".:$HOME/.bookmarks:/"
	alias goto="cd -P"
fi

# }}}

# FUNCTIONS -------------------------------------------------------------- {{{

# Function to help with examples of commands ###
cheat() {
	curl cheat.sh/"$1"
}

# }}}

# HISTORY ---------------------------------------------------------------- {{{

HISTCONTROL=ignoreboth
shopt -s histappend
HISTSIZE=1000
HISTFILESIZE=2000
HISTFILE="$HOME/.bash_history"

# }}}

# COLORS AND UI ---------------------------------------------------------- {{{

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
	if test -r ~/.dircolors; then
		eval "$(dircolors -b ~/.dircolors)"
	else
		eval "$(dircolors -b)"
	fi
	alias ls='ls --color=auto'
	alias grep='grep --color=auto'
	alias fgrep='fgrep --color=auto'
	alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# }}}

# PROMPT ----------------------------------------------------------------- {{{

PROMPT_LEFT_BRACKET=$'\[\e[1;34m\][\[\e[0m\]'
PROMPT_USER=$'\[\e[1;37m\]\u\[\e[m\]'
PROMPT_AT=$'\[\e[1;31m\]@\[\e[m\]'
PROMPT_HOSTNAME=$'\[\e[1;37m\]\h\[\e[m\]'
PROMPT_RIGHT_BRACKET=$'\[\e[1;34m\]]\[\e[0m\] '
PROMPT_ARROW=$'\[\e[1;32m\]\u279C\[\e[m\]  '
PROMPT_CURRENT_PATH=$'\[\e[1;36m\]\W\[\e[0m\] '

PS1="${PROMPT_LEFT_BRACKET}${PROMPT_USER}${PROMPT_AT}${PROMPT_HOSTNAME}${PROMPT_RIGHT_BRACKET}${PROMPT_ARROW}${PROMPT_CURRENT_PATH}"

# }}}

# ALIASES ---------------------------------------------------------------- {{{

# ls aliases
alias ll='ls -lh'
alias la='ls -lAh'
alias l='ls -lah'

# Git aliases
alias g='git'
alias ga='git add'
alias gcmsg='git commit -m'
alias gp='git push'
alias gst='git status'

# More safe aliases
alias mv='mv -i'
alias rm='rm -i'
alias cp='cp -i'

# Add an "alert" alias for long running cmmands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# }}}

# BASH COMPLETIONS ------------------------------------------------------- {{{

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
	if [ -f /usr/share/bash-completion/bash_completion ]; then
		. /usr/share/bash-completion/bash_completion
	elif [ -f /etc/bash_completion ]; then
		. /etc/bash_completion
	fi
fi

# }}}
